# DigiVR Project
 
DigiVR is a project aimed to port GoogleVR system to Godot Engine. It's aimed now as a plugin, so feel free to give feedback.
 
## TODO Things
 
* Add Distortion effects (shaders, etc)
* Add better gyroscope calibration (collaborations about algorythms?)
* Add better FOV support (Godot MUST expose Camera frustum)
* Add more devices and screens profiles
* Add convergence (if needed)
 
 
